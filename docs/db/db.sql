CREATE sequence next_id;

CREATE
OR REPLACE FUNCTION public.next_id()
 RETURNS bigint
 LANGUAGE plpgsql
AS $function$
DECLARE
seq_id bigint;
BEGIN
SELECT nextval('public.next_id')
INTO seq_id;
return seq_id;
END;
$function$
;

CREATE TYPE user_type AS ENUM (
	'ADMIN',
	'USER'
);

CREATE TABLE public.user
(
    user_id       int8         NOT NULL DEFAULT next_id(),
    fullname      varchar(80)  NOT NULL,
    no_hp         varchar(20)  NOT NULL,
    email         varchar(200) NOT NULL,
    user_type     user_type    NOT NULL,
    username      varchar(20)  NOT NULL,
    passwd        varchar(200) NOT NULL,
    photo_id      int8         NOT NULL,
    is_active     bool         NOT NULL,
    last_login_dt timestamptz(0) NOT NULL,
    pass_version  int4         NOT NULL,
    create_by     int8         NOT NULL,
    create_dt     timestamptz(0) NOT NULL,
    update_by     int8         NOT NULL,
    update_dt     timestamptz(0) NOT NULL,
    delete_by     int8 NULL,
    delete_dt     timestamptz(0) NULL,
    CONSTRAINT user_pk PRIMARY KEY (user_id)
);

CREATE TABLE public.mskanji
(
    mskanji_id        int8         NOT NULL DEFAULT next_id(),
    kanji             varchar(10)  NOT NULL,
    list_meaning      varchar(160)[] NOT NULL,
    list_kun_reading  varchar(160)[] NOT NULL,
    list_on_reading   varchar(160)[] NOT NULL,
    list_name_reading varchar(160)[] NOT NULL,
    grade             varchar(10)  NOT NULL,
    stroke_count      varchar(10)  NOT NULL,
    jlpt              varchar(10)  NOT NULL,
    heisig_en         varchar(100) NOT NULL,
    unicode           varchar(100) NOT NULL,
    create_by         int8         NOT NULL,
    create_dt         timestamptz(0) NOT NULL,
    update_by         int8         NOT NULL,
    update_dt         timestamptz(0) NOT NULL,
    CONSTRAINT mskanji_pk PRIMARY KEY (mskanji_id)
);

CREATE TABLE public.mskanjiexample
(
    mskanjiexample_id int8         NOT NULL DEFAULT next_id(),
    mskanji_id        int8         NOT NULL,
    kanjiexample      varchar(80)  NOT NULL,
    kana              varchar(80)  NOT NULL,
    mean              varchar(80)  NOT NULL,
    description       varchar(500) NOT NULL,
    create_by         int8         NOT NULL,
    create_dt         timestamptz(0) NOT NULL,
    update_by         int8         NOT NULL,
    update_dt         timestamptz(0) NOT NULL,
    CONSTRAINT mskanjiexample_pk PRIMARY KEY (mskanjiexample_id)
);

CREATE TABLE public.kanji
(
    kanji_id          int8         NOT NULL DEFAULT next_id(),
    mskanji_id        int8         NOT NULL,
    user_id           int8         NOT NULL,
    kanji             varchar(10)  NOT NULL,
    list_meaning      varchar(160)[] NOT NULL,
    list_kun_reading  varchar(160)[] NOT NULL,
    list_on_reading   varchar(160)[] NOT NULL,
    list_name_reading varchar(160)[] NOT NULL,
    grade             varchar(10)  NOT NULL,
    stroke_count      varchar(10)  NOT NULL,
    jlpt              varchar(100) NOT NULL,
    heisig_en         varchar(100) NOT NULL,
    unicode           varchar(10)  NOT NULL,
    create_by         int8         NOT NULL,
    create_dt         timestamptz(0) NOT NULL,
    update_by         int8         NOT NULL,
    update_dt         timestamptz(0) NOT NULL,
    CONSTRAINT kanji_pk PRIMARY KEY (kanji_id)
);

CREATE TABLE public.kanjiexample
(
    kanjiexample_id int8         NOT NULL DEFAULT next_id(),
    mskanji_id      int8         NOT NULL,
    kanji_id        int8         NOT NULL,
    user_id         int8         NOT NULL,
    kanjiexample    varchar(80)  NOT NULL,
    kana            varchar(80)  NOT NULL,
    mean            varchar(80)  NOT NULL,
    description     varchar(500) NOT NULL,
    create_by       int8         NOT NULL,
    create_dt       timestamptz(0) NOT NULL,
    update_by       int8         NOT NULL,
    update_dt       timestamptz(0) NOT NULL,
    CONSTRAINT kanjiexample_pk PRIMARY KEY (kanjiexample_id)
);

CREATE TABLE public.photo
(
    photo_id     int8         NOT NULL DEFAULT next_id(),
    client_name  varchar(200) NOT NULL,
    server_name  varchar(200) NOT NULL,
    ext          varchar(5)   NOT NULL,
    photo_path   varchar(200) NOT NULL,
    photo_size   int8         NOT NULL,
    photo_width  int8         NOT NULL,
    photo_heigth int8         NOT NULL,
    create_by    int8         NOT NULL,
    create_dt    timestamptz(0) NOT NULL,
    CONSTRAINT photo_pk PRIMARY KEY (photo_id)
);

CREATE TABLE public.msword
(
    msword_id   int8         NOT NULL DEFAULT next_id(),
    list_msword varchar(80)[] NOT NULL,
    word        varchar(80)  NOT NULL,
    kana        varchar(80)  NOT NULL,
    mean        varchar(80)  NOT NULL,
    description varchar(500) NOT NULL,
    create_by   int8         NOT NULL,
    create_dt   timestamptz(0) NOT NULL,
    update_by   int8         NOT NULL,
    update_dt   timestamptz(0) NOT NULL,
    CONSTRAINT msword_pk PRIMARY KEY (msword_id)
);

