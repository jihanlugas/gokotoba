package constant

import (
	"gokotoba/config"
	"os"
)

type DocType uint8

const (
	FOTO_USER DocType = iota
)

var FolderForFile map[DocType]string

func init() {
	FolderForFile = make(map[DocType]string)
	FolderForFile[FOTO_USER] = "user"
}

func CreateUploadFolder() error {
	var err error
	for _, folderPath := range FolderForFile {
		folderPath = config.UploadFilePath + "/" + folderPath
		if _, err = os.Stat(folderPath); os.IsNotExist(err) {
			err = os.MkdirAll(folderPath, 0755)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
