package controller

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"

	"gokotoba/db"
	"gokotoba/model"
	"gokotoba/response"
)

type Mskanji struct{}

func MskanjiComposer() Mskanji {
	return Mskanji{}
}

type searchReq struct {
	Kanji string `json:"kanji" form:"kanji" query:"kanji" validate:"required,len=1,kanji" example:""`
}

type updateMskanjiReq struct {
	MskanjiID          int64                     `json:"mskanjiId" form:"mskanjiId" query:"mskanjiId" validate:"required,existsdata=mskanji_id mskanjiId"`
	Kanji              string                    `json:"kanji" form:"kanji" query:"kanji" validate:"required,lte=10"`
	ListMeaning        []string                  `json:"listMeaning" form:"listMeaning" query:"listMeaning" validate:"required"`
	ListKunReading     []string                  `json:"listKunReading" form:"listKunReading" query:"listKunReading" validate:"required"`
	ListOnReading      []string                  `json:"listOnReading" form:"listOnReading" query:"listOnReading" validate:"required"`
	ListNameReading    []string                  `json:"listNameReading" form:"listNameReading" query:"listNameReading" validate:"required"`
	Grade              string                    `json:"grade" form:"grade" query:"grade" validate:"required,number,lte=10"`
	StrokeCount        string                    `json:"strokeCount" form:"strokeCount" query:"strokeCount" validate:"required,number,lte=10"`
	Jlpt               string                    `json:"jlpt" form:"jlpt" query:"jlpt" validate:"required,lte=10"`
	HeisigEn           string                    `json:"heisigEn" form:"heisigEn" query:"heisigEn" validate:"lte=10"`
	Unicode            string                    `json:"unicode" form:"unicode" query:"unicode" validate:"required,lte=10"`
	ListMskanjiExample []mskanjiexampleUpdateReq `json:"listMskanjiExample" form:"listMskanjiExample" query:"listMskanjiExample" validate:"dive"`
}

// @Tags Mskanji
// @Summary get mskanji by id
// @Accept json
// @Produce json
// @in header
// @Param mskanji_id path integer true "mskanji id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.MskanjiDetailRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji/id/{mskanji_id} [get]
func (h Mskanji) Get(c echo.Context) error {
	var err error
	var mskanji model.PublicMskanji

	reqID, err := strconv.ParseInt(c.Param("mskanji_id"), 10, 64)
	if err != nil {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	mskanji.MskanjiID = reqID
	err = mskanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	q := model.GetMskanjiexampleQuery().Where().
		Int64("mskanji_id", "=", mskanji.MskanjiID)
	listMskanjiexample, err := model.GetMskanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.MskanjiDetailRes{
		MskanjiRes:         mskanji.Res(),
		ListMskanjiExample: model.ToMskanjiexampleRes(listMskanjiexample),
	}

	return response.Success(response.ResponseSuccess, res).SendJSON(c)
}

// @Tags Mskanji
// @Summary get mskanji by kanji
// @Accept json
// @Produce json
// @in header
// @Param kanji path string true "kanji" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.MskanjiDetailRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji/kanji/{kanji} [get]
func (h Mskanji) GetByKanji(c echo.Context) error {
	var err error
	var mskanji model.PublicMskanji

	reqKanji, err := url.QueryUnescape(c.Param("kanji"))
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	mskanji.Kanji = reqKanji
	err = mskanji.GetByKanji(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	q := model.GetMskanjiexampleQuery().Where().
		Int64("mskanji_id", "=", mskanji.MskanjiID)
	listMskanjiexample, err := model.GetMskanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.MskanjiDetailRes{
		MskanjiRes:         mskanji.Res(),
		ListMskanjiExample: model.ToMskanjiexampleRes(listMskanjiexample),
	}

	return response.Success(response.ResponseSuccess, res).SendJSON(c)
}

// @Tags Mskanji
// @Summary search kanji
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query searchReq true "Req Param"
// @Success 200 {object} response.SuccessResponse{payload=model.MskanjiDetailRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji/search [get]
func (h Mskanji) Search(c echo.Context) error {
	var err error
	var mskanji model.PublicMskanji

	req := new(searchReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	mskanji.Kanji = req.Kanji
	err = mskanji.GetByKanji(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	q := model.GetMskanjiexampleQuery().Where().
		Int64("mskanji_id", "=", mskanji.MskanjiID)
	listMskanjiexample, err := model.GetMskanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.MskanjiDetailRes{
		MskanjiRes:         mskanji.Res(),
		ListMskanjiExample: model.ToMskanjiexampleRes(listMskanjiexample),
	}

	return response.Success(response.ResponseSuccess, res).SendJSON(c)
}

// @Tags Mskanji
// @Summary update mskanji
// @Description update mskanji and mskanjiexample
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body updateMskanjiReq true "Req Param"
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji/update [post]
func (h Mskanji) Update(c echo.Context) error {
	var err error
	var mskanji model.PublicMskanji
	var mskanjiexample model.PublicMskanjiexample

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(updateMskanjiReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	mskanji.MskanjiID = req.MskanjiID
	err = mskanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	mskanji.ListMeaning = req.ListMeaning
	mskanji.ListKunReading = req.ListKunReading
	mskanji.ListOnReading = req.ListOnReading
	mskanji.ListNameReading = req.ListNameReading
	mskanji.Grade = req.Grade
	mskanji.StrokeCount = req.StrokeCount
	mskanji.Jlpt = req.Jlpt
	mskanji.HeisigEn = req.HeisigEn
	mskanji.Unicode = req.Unicode
	mskanji.UpdateBy = loginUser.UserID
	err = mskanji.Update(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	notDeleteMskanjiexampleID := []int64{}
	for _, reqmskanjiexample := range req.ListMskanjiExample {
		mskanjiexample.MskanjiexampleID = reqmskanjiexample.MskanjiexampleID
		if mskanjiexample.MskanjiexampleID != 0 {
			err = mskanjiexample.GetById(ctx, conn)
			if err != nil {
				if errors.Is(err, pgx.ErrNoRows) {
					return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
				}
				errorInternal(c, err)
			}
			mskanjiexample.MskanjiID = mskanji.MskanjiID
			mskanjiexample.Kanjiexample = reqmskanjiexample.Kanjiexample
			mskanjiexample.Kana = reqmskanjiexample.Kana
			mskanjiexample.Mean = reqmskanjiexample.Mean
			mskanjiexample.Description = reqmskanjiexample.Description
			mskanjiexample.UpdateBy = loginUser.UserID
			err = mskanjiexample.Update(ctx, tx)
			if err != nil {
				errorInternal(c, err)
			}
		} else {
			mskanjiexample.MskanjiID = mskanji.MskanjiID
			mskanjiexample.Kanjiexample = reqmskanjiexample.Kanjiexample
			mskanjiexample.Kana = reqmskanjiexample.Kana
			mskanjiexample.Mean = reqmskanjiexample.Mean
			mskanjiexample.Description = reqmskanjiexample.Description
			mskanjiexample.CreateBy = loginUser.UserID
			mskanjiexample.UpdateBy = loginUser.UserID
			err = mskanjiexample.Insert(ctx, tx)
			if err != nil {
				errorInternal(c, err)
			}
		}
		notDeleteMskanjiexampleID = append(notDeleteMskanjiexampleID, mskanjiexample.MskanjiexampleID)
	}

	_, err = tx.Exec(ctx, `DELETE FROM public.mskanjiexample WHERE mskanji_id = $1
		AND mskanjiexample_id != all ($2)`,
		mskanji.MskanjiID,
		notDeleteMskanjiexampleID,
	)

	if err != nil {
		errorInternal(c, err)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success("Success ", response.Payload{}).SendJSON(c)
}

// @Tags Mskanji
// @Summary fetch kanji from api and save to database
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji/fetch [get]
func (h Mskanji) Fetch(c echo.Context) error {
	var err error
	var ListKanji []string

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	url := "https://kanjiapi.dev/v1/kanji/all"
	spaceClient := http.Client{
		Timeout: time.Second * 10, // Timeout after 2 seconds
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		errorInternal(c, err)
	}
	req.Header.Set("Accept", "application/json")
	res, err := spaceClient.Do(req)
	if err != nil {
		errorInternal(c, err)
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		errorInternal(c, err)
	}

	err = json.Unmarshal(body, &ListKanji)
	if err != nil {
		errorInternal(c, err)
	}

	// conn, ctx, closeConn := db.GetConnection()
	// defer closeConn()

	// tx, err := conn.Begin(ctx)
	// if err != nil {
	// 	errorInternal(c, err)
	// }
	// defer db.DeferHandleTransaction(ctx, tx)

	for i, newkanji := range ListKanji {
		if i < 15000 {
			err = doFetch(newkanji, loginUser.UserID)
			if err != nil {
				errorInternal(c, err)
			}
		}
	}

	// if err = tx.Commit(ctx); err != nil {
	// 	_ = tx.Rollback(ctx)
	// 	errorInternal(c, err)
	// }

	return response.Success("Success", response.Payload{}).SendJSON(c)
}

// @Tags Mskanji
// @Summary Add mskanji to kanji
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param mskanji_id path string true "mskanji id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji/addtokanji/{mskanji_id} [get]
func (h Mskanji) AddToKanji(c echo.Context) error {
	var err error
	var mskanji model.PublicMskanji
	var kanji model.PublicKanji
	var listMskanjiexample []model.PublicMskanjiexample
	var listKanjiexample []model.PublicKanjiexample
	var kanjiexample model.PublicKanjiexample

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	reqID, err := strconv.ParseInt(c.Param("mskanji_id"), 10, 64)
	if err != nil {
		return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	mskanji.MskanjiID = reqID
	err = mskanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	q := model.GetMskanjiexampleQuery().Where().
		Int64("mskanji_id", "=", mskanji.MskanjiID)
	listMskanjiexample, err = model.GetMskanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	kanji.MskanjiID = mskanji.MskanjiID
	kanji.UserID = loginUser.UserID
	err = kanji.GetByUserMskanji(ctx, conn)
	if err != nil {
		if !errors.Is(err, pgx.ErrNoRows) {
			errorInternal(c, err)
		}
	} else {
		return response.Error("Kanji sudah ada", response.Payload{}).SendJSON(c)
	}

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	kanji.Kanji = mskanji.Kanji
	kanji.ListMeaning = mskanji.ListMeaning
	kanji.ListKunReading = mskanji.ListKunReading
	kanji.ListOnReading = mskanji.ListOnReading
	kanji.ListNameReading = mskanji.ListNameReading
	kanji.Grade = mskanji.Grade
	kanji.StrokeCount = mskanji.StrokeCount
	kanji.Jlpt = mskanji.Jlpt
	kanji.HeisigEn = mskanji.HeisigEn
	kanji.Unicode = mskanji.Unicode
	kanji.CreateBy = loginUser.UserID
	kanji.UpdateBy = loginUser.UserID
	err = kanji.Insert(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	for _, mskanjiexample := range listMskanjiexample {
		kanjiexample.MskanjiID = mskanjiexample.MskanjiID
		kanjiexample.KanjiID = kanji.KanjiID
		kanjiexample.UserID = kanji.UserID
		kanjiexample.Kanjiexample = mskanjiexample.Kanjiexample
		kanjiexample.Kana = mskanjiexample.Kana
		kanjiexample.Mean = mskanjiexample.Mean
		kanjiexample.Description = mskanjiexample.Description
		kanjiexample.CreateBy = loginUser.UserID
		kanjiexample.UpdateBy = loginUser.UserID
		err = kanjiexample.Insert(ctx, tx)
		if err != nil {
			errorInternal(c, err)
		}

		listKanjiexample = append(listKanjiexample, kanjiexample)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, kanji.Res()).SendJSON(c)
}

func doFetch(newkanji string, UserID int64) error {
	var kanji model.PublicMskanji

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		return err
	}
	defer db.DeferHandleTransaction(ctx, tx)

	kanji.Kanji = newkanji
	err = kanji.GetByKanji(ctx, conn)
	if err != nil {
		if !errors.Is(err, pgx.ErrNoRows) {
			return err
		}
	} else {
		return nil
	}

	err = kanji.FetchKanji()
	if err != nil {
		return err
	}

	kanji.CreateBy = UserID
	kanji.UpdateBy = UserID
	err = kanji.Insert(ctx, tx)
	if err != nil {
		return err
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		return err
	}

	return nil
}
