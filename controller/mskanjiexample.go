package controller

import (
	"errors"
	"strconv"

	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"

	"gokotoba/constant"
	"gokotoba/db"
	"gokotoba/model"
	"gokotoba/response"
)

type Mskanjiexample struct{}

func MskanjiexampleComposer() Mskanjiexample {
	return Mskanjiexample{}
}

type mskanjiexampleListReq struct {
	MskanjiID    int64  `json:"mskanjiId" validate:""`
	Kanjiexample string `json:"kanjiexample" validate:""`
	Kana         string `json:"kana" validate:"kana"`
	Mean         string `json:"mean" validate:""`
	Description  string `json:"description" validate:""`
}

type mskanjiexampleCreateReq struct {
	MskanjiID    int64  `json:"mskanjiId"  validate:"required,existsdata=mskanji_id mskanjiId"`
	Kanjiexample string `json:"kanjiexample"  validate:"required"`
	Kana         string `json:"kana"  validate:"required,kana"`
	Mean         string `json:"mean"  validate:"required"`
	Description  string `json:"description"  validate:""`
}

type mskanjiexampleUpdateReq struct {
	MskanjiexampleID int64  `json:"mskanjiexampleId" form:"mskanjiexampleId" query:"mskanjiexampleId" validate:"existsdata=mskanjiexample_id mskanjiexampleId"`
	Kanjiexample     string `json:"kanjiexample" form:"kanjiexample" query:"kanjiexample" validate:"required"`
	Kana             string `json:"kana" form:"kana" query:"kana" validate:"required,kana"`
	Mean             string `json:"mean" form:"mean" query:"mean" validate:"required"`
	Description      string `json:"description" form:"description" query:"description" validate:""`
}

// @Tags Mskanjiexample Example
// @Summary Get Mskanjiexample Example
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param mskanjiexample_id path string true "Mskanjiexample id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.MskanjiexampleRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji-example/{mskanjiexample_id} [get]
func (h Mskanjiexample) Get(c echo.Context) error {
	var err error
	var mskanjiexample model.PublicMskanjiexample

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	if loginUser.UserType != constant.UserTypeAdmin {
		return response.Error(response.ResponseNotAllowed, response.Payload{}).SendJSON(c)
	}

	reqID, err := strconv.ParseInt(c.Param("mskanjiexample_id"), 10, 64)
	if err != nil {
		return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	mskanjiexample.MskanjiexampleID = reqID
	err = mskanjiexample.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, mskanjiexample.Res()).SendJSON(c)
}

// @Tags Mskanjiexample Example
// @Summary List Mskanjiexample Example
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body mskanjiexampleListReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=[]model.MskanjiexampleRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji-example/list [post]
func (h Mskanjiexample) List(c echo.Context) error {
	var err error
	var listMskanjiexample []model.PublicMskanjiexample

	req := new(mskanjiexampleListReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	q := model.GetMskanjiexampleQuery().Where().
		Int64Skip("mskanji_id", "=", req.MskanjiID).
		StringLike("kanjiexample", req.Kanjiexample).
		StringLike("kana", req.Kana).
		StringLike("mean", req.Mean).
		StringLike("description", req.Description)
	listMskanjiexample, err = model.GetMskanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.ToMskanjiexampleRes(listMskanjiexample)

	return response.Success("Success", res).SendJSON(c)
}

// @Tags Mskanjiexample Example
// @Summary Create Mskanjiexample Example
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body mskanjiexampleCreateReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=model.MskanjiexampleRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /mskanji-example/create [post]
func (h Mskanjiexample) Create(c echo.Context) error {
	var err error
	var mskanjiexample model.PublicMskanjiexample

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(mskanjiexampleCreateReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	mskanjiexample.MskanjiID = req.MskanjiID
	mskanjiexample.Kanjiexample = req.Kanjiexample
	mskanjiexample.Kana = req.Kana
	mskanjiexample.Mean = req.Mean
	mskanjiexample.Description = req.Description
	mskanjiexample.CreateBy = loginUser.UserID
	mskanjiexample.UpdateBy = loginUser.UserID
	err = mskanjiexample.Insert(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, mskanjiexample.Res()).SendJSON(c)
}
