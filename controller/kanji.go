package controller

import (
	"errors"
	"gokotoba/db"
	"gokotoba/model"
	"gokotoba/response"
	"net/url"
	"strconv"

	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
)

type Kanji struct{}

func KanjiComposer() Kanji {
	return Kanji{}
}

type kanjiexampleUpdateReq struct {
	KanjiexampleID int64  `json:"kanjiexampleId" form:"kanjiexampleId" query:"kanjiexampleId" validate:"existsdata=kanjiexample_id kanjiexampleId"`
	KanjiID        int64  `json:"kanjiId" form:"kanjiId" query:"kanjiId" validate:"required"`
	Kanjiexample   string `json:"kanjiexample" form:"kanjiexample" query:"kanjiexample" validate:"required,lte=80"`
	Kana           string `json:"kana" form:"kana" query:"kana" validate:"required,lte=80"`
	Mean           string `json:"mean" form:"mean" query:"mean" validate:"required,lte=80"`
	Description    string `json:"description" form:"description" query:"description" validate:"lte=200"`
}

type updateKanjiReq struct {
	KanjiID          int64                   `json:"kanjiId" form:"kanjiId" query:"kanjiId" validate:"required,existsdata=kanji_id kanjiId"`
	ListMeaning      []string                `json:"listMeaning" form:"listMeaning" query:"listMeaning" validate:"required"`
	ListKunReading   []string                `json:"listKunReading" form:"listKunReading" query:"listKunReading" validate:"required"`
	ListOnReading    []string                `json:"listOnReading" form:"listOnReading" query:"listOnReading" validate:"required"`
	ListNameReading  []string                `json:"listNameReading" form:"listNameReading" query:"listNameReading" validate:"required"`
	Grade            string                  `json:"grade" form:"grade" query:"grade" validate:"required,number,lte=10"`
	StrokeCount      string                  `json:"strokeCount" form:"strokeCount" query:"strokeCount" validate:"required,number,lte=10"`
	Jlpt             string                  `json:"jlpt" form:"jlpt" query:"jlpt" validate:"required,lte=10"`
	HeisigEn         string                  `json:"heisigEn" form:"heisigEn" query:"heisigEn" validate:"lte=10"`
	Unicode          string                  `json:"unicode" form:"unicode" query:"unicode" validate:"required,lte=10"`
	ListKanjiExample []kanjiexampleUpdateReq `json:"listKanjiExample" form:"listKanjiExample" query:"listKanjiExample" validate:"dive"`
}

type kanjiListReq struct {
}

// @Tags Kanji
// @Summary get kanji
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param kanji path string true "kanji"
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiDetailRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/{kanji} [get]
func (h Kanji) GetByKanji(c echo.Context) error {
	var err error
	var kanji model.PublicKanji
	var listKanjiexample []model.PublicKanjiexample

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	//reqKanji := c.Param("kanji")
	reqKanji, err := url.QueryUnescape(c.Param("kanji"))
	if err != nil {
		return response.Error("Path Not Found", response.Payload{}).SendJSON(c)
	}

	if err != nil {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.Kanji = reqKanji
	kanji.UserID = loginUser.UserID
	err = kanji.GetByKanji(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	q := model.GetKanjiexampleQuery().Where().
		Int64("kanji_id", "=", kanji.KanjiID)
	listKanjiexample, err = model.GetKanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.KanjiDetailRes{
		KanjiRes:         kanji.Res(),
		ListKanjiExample: model.ToKanjiexampleRes(listKanjiexample),
	}

	return response.Success(response.ResponseSuccess, res).SendJSON(c)
}

// @Tags Kanji
// @Summary get kanji by id
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param kanji_id path string true "kanji id" default(0)
// @Success 200 {object} response.SuccessResponse{payload=model.KanjiDetailRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/id/{kanji_id} [get]
func (h Kanji) Get(c echo.Context) error {
	var err error
	var kanji model.PublicKanji
	var listKanjiexample []model.PublicKanjiexample

	// loginUser, err := getUserLoginInfo(c)
	// if err != nil {
	// 	errorInternal(c, err)
	// }

	reqID, err := strconv.ParseInt(c.Param("kanji_id"), 10, 64)
	if err != nil {
		return response.Error("Not Found", response.Payload{}).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.KanjiID = reqID
	err = kanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	q := model.GetKanjiexampleQuery().Where().
		Int64("kanji_id", "=", kanji.KanjiID)
	listKanjiexample, err = model.GetKanjiexampleWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.KanjiDetailRes{
		KanjiRes:         kanji.Res(),
		ListKanjiExample: model.ToKanjiexampleRes(listKanjiexample),
	}

	return response.Success(response.ResponseSuccess, res).SendJSON(c)
}

// @Tags Kanji
// @Summary update kanji
// @Description update kanji and kanjiexample
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body updateKanjiReq true "Req Param"
// @Success 200 {object} response.SuccessResponse "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/update [post]
func (h Kanji) Update(c echo.Context) error {
	var err error
	var kanji model.PublicKanji
	var kanjiexample model.PublicKanjiexample
	var notDeleteKanjiexampleID []int64

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(updateKanjiReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	kanji.KanjiID = req.KanjiID
	err = kanji.GetById(ctx, conn)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
		}
		errorInternal(c, err)
	}

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	kanji.ListMeaning = req.ListMeaning
	kanji.ListKunReading = req.ListKunReading
	kanji.ListOnReading = req.ListOnReading
	kanji.ListNameReading = req.ListNameReading
	kanji.Grade = req.Grade
	kanji.StrokeCount = req.StrokeCount
	kanji.Jlpt = req.Jlpt
	kanji.HeisigEn = req.HeisigEn
	kanji.Unicode = req.Unicode
	kanji.UpdateBy = loginUser.UserID
	err = kanji.Update(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	for _, reqkanjiexample := range req.ListKanjiExample {
		kanjiexample.KanjiexampleID = reqkanjiexample.KanjiexampleID
		if kanjiexample.KanjiexampleID != 0 {
			err = kanjiexample.GetById(ctx, conn)
			if err != nil {
				if errors.Is(err, pgx.ErrNoRows) {
					return response.Error(response.ResponseDataNotFound, response.Payload{}).SendJSON(c)
				}
				errorInternal(c, err)
			}
			kanjiexample.KanjiID = reqkanjiexample.KanjiID
			kanjiexample.Kanjiexample = reqkanjiexample.Kanjiexample
			kanjiexample.Kana = reqkanjiexample.Kana
			kanjiexample.Mean = reqkanjiexample.Mean
			kanjiexample.Description = reqkanjiexample.Description
			kanjiexample.UpdateBy = loginUser.UserID
			err = kanjiexample.Update(ctx, tx)
			if err != nil {
				errorInternal(c, err)
			}
		} else {
			kanjiexample.KanjiID = reqkanjiexample.KanjiID
			kanjiexample.MskanjiID = kanji.MskanjiID
			kanjiexample.UserID = loginUser.UserID
			kanjiexample.Kanjiexample = reqkanjiexample.Kanjiexample
			kanjiexample.Kana = reqkanjiexample.Kana
			kanjiexample.Mean = reqkanjiexample.Mean
			kanjiexample.Description = reqkanjiexample.Description
			kanjiexample.CreateBy = loginUser.UserID
			kanjiexample.UpdateBy = loginUser.UserID
			err = kanjiexample.Insert(ctx, tx)
			if err != nil {
				errorInternal(c, err)
			}
		}

		notDeleteKanjiexampleID = append(notDeleteKanjiexampleID, kanjiexample.KanjiexampleID)
	}

	_, err = tx.Exec(ctx, `DELETE FROM public.kanjiexample WHERE kanji_id = $1
		AND kanjiexample_id != all ($2)`,
		kanji.KanjiID,
		notDeleteKanjiexampleID,
	)
	if err != nil {
		errorInternal(c, err)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success("Success ", req).SendJSON(c)
}

// @Tags Kanji
// @Summary List Kanji Example
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query kanjiListReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=[]model.KanjiRes} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /kanji/list [get]
func (h Kanji) List(c echo.Context) error {
	var err error
	var listKanji []model.PublicKanji

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(kanjiListReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	q := model.GetKanjiQuery().Where().
		Int64("user_id", "=", loginUser.UserID)
	listKanji, err = model.GetKanjiWhere(ctx, conn, q)
	if err != nil {
		errorInternal(c, err)
	}

	res := model.ToKanjiRes(listKanji)

	return response.Success("Success", res).SendJSON(c)

}
