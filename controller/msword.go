package controller

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"gokotoba/db"
	"gokotoba/model"
	"gokotoba/request"
	"gokotoba/response"
)

type Msword struct{}

func MswordComposer() Msword {
	return Msword{}
}

type createMswordReq struct {
	Word        string `json:"word" form:"word" query:"word" validate:"required,lte=80,kanji,notexists=word word"`
	Kana        string `json:"kana" form:"kana" query:"kana" validate:"required,lte=80,kana"`
	Mean        string `json:"mean" form:"mean" query:"mean" validate:"required,lte=80"`
	Description string `json:"description" form:"description" query:"description" validate:"lte=500"`
}

type pageMswordReq struct {
	request.Paging
}

// @Tags Msword
// @Summary Page Msword
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req query pageMswordReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=model.MswordResDetail} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /msword [get]
func (h Msword) Page(c echo.Context) error {
	var err error

	req := new(pageMswordReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	fmt.Println("req ", req)

	err, cnt, list := getMswordPage(req)
	if err != nil {
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, response.PayloadPagination(req, list, cnt)).SendJSON(c)
}

func getMswordPage(req *pageMswordReq) (error, int, []model.PublicMsword) {
	var err error
	var cnt int
	var list []model.PublicMsword

	q := model.GetMswordQuery().Where()

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	// get total page
	list, err = model.GetMswordWhere(ctx, conn, q)
	if err != nil {
		return err, cnt, list
	}
	cnt = len(list)

	// get data
	if req.GetPage() < 1 {
		req.SetPage(1)
	}

	list = make([]model.PublicMsword, 0)
	if req.GetPage() > 1 {
		q.OffsetLimit((req.GetPage()-1)*req.GetLimit(), req.GetLimit())
	} else {
		q.OffsetLimit(0, req.GetLimit())
	}
	list, err = model.GetMswordWhere(ctx, conn, q)
	if err != nil {
		return err, cnt, list
	}

	return err, cnt, list
}

func setPageLimitAndSort(q *db.QueryBuilder, req request.IPaging) error {
	var err error

	return err
}

// @Tags Msword
// @Summary Create Msword
// @Accept json
// @Produce json
// @in header
// @name Authorization
// @Param req body createMswordReq false "json request body"
// @Success 200 {object} response.SuccessResponse{payload=model.MswordResDetail} "json with success = true"
// @Failure 400 {object} response.ErrorResponse "json with error = true"
// @Router /msword/create [post]
func (h Msword) Create(c echo.Context) error {
	var err error
	var msword model.PublicMsword

	loginUser, err := getUserLoginInfo(c)
	if err != nil {
		errorInternal(c, err)
	}

	req := new(createMswordReq)
	if err = c.Bind(req); err != nil {
		return err
	}

	if err = c.Validate(req); err != nil {
		return response.Error(response.ResponseValidationFailed, response.ValidationError(err)).SendJSON(c)
	}

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	tx, err := conn.Begin(ctx)
	if err != nil {
		errorInternal(c, err)
	}
	defer db.DeferHandleTransaction(ctx, tx)

	msword.Word = req.Word
	msword.Kana = req.Kana
	msword.Mean = req.Mean
	msword.Description = req.Description
	msword.CreateBy = loginUser.UserID
	msword.UpdateBy = loginUser.UserID
	err = msword.Insert(ctx, tx)
	if err != nil {
		errorInternal(c, err)
	}

	if err = tx.Commit(ctx); err != nil {
		_ = tx.Rollback(ctx)
		errorInternal(c, err)
	}

	return response.Success(response.ResponseSuccess, msword.ResDetail()).SendJSON(c)

}
