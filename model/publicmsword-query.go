//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gokotoba/db"
	"time"
)

type MswordRes struct {
	MswordID    int64    `json:"mswordId"`
	ListMsword  []string `json:"listMsword"`
	Word        string   `json:"word"`
	Kana        string   `json:"kana"`
	Mean        string   `json:"mean"`
	Description string   `json:"description"`
}

type MswordResDetail struct {
	MswordID    int64                `json:"mswordId"`
	ListMsword  []string             `json:"listMsword"`
	Word        string               `json:"word"`
	Kana        string               `json:"kana"`
	Mean        string               `json:"mean"`
	Description string               `json:"description"`
	Mskanjis    map[int64]MskanjiRes `json:"mskanjis"`
}

func GetMswordQuery() *db.QueryComposer {
	return db.Query(`SELECT msword_id, list_msword, word, kana, mean, description, create_by, create_dt, update_by, update_dt FROM public.msword`)
}

func (p *PublicMsword) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetMswordQuery().
		Where().
		Int64(`msword_id`, "=", int64(p.MswordID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicMsword) GetByWord(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetMswordQuery().
		Where().
		StringEq("word", p.Word).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func GetMswordWhere(ctx context.Context, conn *pgxpool.Conn, q *db.QueryBuilder) ([]PublicMsword, error) {
	var err error
	var data []PublicMsword

	err = pgxscan.Select(ctx, conn, &data, q.Build(), q.Params()...)
	if err != nil {
		return data, err
	}
	if len(data) == 0 {
		data = make([]PublicMsword, 0)
	}

	return data, err
}

func (p *PublicMsword) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	// map list_word based word
	chars := []rune(p.Word)
	for i := 0; i < len(chars); i++ {
		p.ListMsword = append(p.ListMsword, string(chars[i]))
	}

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.msword
		(list_msword, word, kana, mean, description, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
		RETURNING msword_id;`,
		p.ListMsword,
		p.Word,
		p.Kana,
		p.Mean,
		p.Description,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.MswordID)

	return err
}

// word and list_word not update
func (p *PublicMsword) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.msword SET kana = $1
		, mean = $2
		, description = $3
		, update_by = $4
		, update_dt = $5
		WHERE msword_id = $6`,
		p.Kana,
		p.Mean,
		p.Description,
		p.UpdateBy,
		p.UpdateDt,
		p.MswordID,
	)
	return err
}

func (p *PublicMsword) Delete(ctx context.Context, tx pgx.Tx) error {
	var err error

	_, err = tx.Exec(ctx, `DELETE FROM public.msword WHERE msword_id = $1`,
		p.MswordID,
	)
	return err
}

func (p *PublicMsword) Res() MswordRes {
	var res MswordRes

	res.MswordID = p.MswordID
	res.ListMsword = p.ListMsword
	res.Word = p.Word
	res.Kana = p.Kana
	res.Mean = p.Mean
	res.Description = p.Description

	return res
}

func (p *PublicMsword) ResDetail() MswordResDetail {
	var res MswordResDetail
	var mskanji PublicMskanji
	var err error

	conn, ctx, closeConn := db.GetConnection()
	defer closeConn()

	res.MswordID = p.MswordID
	res.ListMsword = p.ListMsword
	res.Word = p.Word
	res.Kana = p.Kana
	res.Mean = p.Mean
	res.Description = p.Description

	res.Mskanjis = make(map[int64]MskanjiRes)
	for _, v := range p.ListMsword {
		mskanji.Kanji = v
		err = mskanji.GetByKanji(ctx, conn)
		if err != nil {
			break
		}
		res.Mskanjis[mskanji.MskanjiID] = mskanji.Res()
	}

	return res
}
