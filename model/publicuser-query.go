package model

import (
	"context"
	"gokotoba/db"
	"strings"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type UserRes struct {
	UserID   int64  `json:"userId"`
	Fullname string `json:"fullname"`
	NoHp     string `json:"noHp"`
	Email    string `json:"email"`
	UserType string `json:"userType"`
	PhotoID  int64  `json:"photoId"`
	IsActive bool   `json:"isActive"`
	PhotoUrl string `json:"photoUrl"`
}

func GetUserQuery() *db.QueryComposer {
	return db.Query(`SELECT user_id, fullname, email, user_type, username, no_hp, passwd, photo_id, is_active, last_login_dt, pass_version, create_by, create_dt, update_by, update_dt FROM public.user`)
}

func (p *PublicUser) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetUserQuery().
		Where().
		Int64(`user_id`, "=", int64(p.UserID)).
		IsNull(`delete_dt`).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicUser) GetByUsername(ctx context.Context, conn *pgxpool.Conn) error {
	var err error
	p.Username = strings.ToLower(p.Username)

	sql := GetUserQuery().
		Where().
		StringEq(`username`, p.Username).
		IsNull(`delete_dt`).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicUser) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.Username = strings.ToLower(p.Username)
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.user (fullname, email, user_type, username, no_hp, passwd, photo_id, is_active, last_login_dt, pass_version, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
		RETURNING user_id;`,
		p.Fullname,
		p.Email,
		p.UserType,
		p.Username,
		p.NoHp,
		p.Passwd,
		p.PhotoID,
		p.IsActive,
		p.LastLoginDt,
		p.PassVersion,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.UserID)
	return err
}

// passwd not updated
func (p *PublicUser) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.Username = strings.ToLower(p.Username)
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.user SET fullname = $1
		, email = $2
		, user_type = $3
		, username = $4
		, no_hp = $5
		, photo_id = $6
		, is_active = $7
		, last_login_dt = $8
		, pass_version = $9
		, update_by = $10
		, update_dt = $11
		WHERE user_id = $12`,
		p.Fullname,
		p.Email,
		p.UserType,
		p.Username,
		p.NoHp,
		p.PhotoID,
		p.IsActive,
		p.LastLoginDt,
		p.PassVersion,
		p.UpdateBy,
		p.UpdateDt,
		p.UserID,
	)
	return err
}

func (p *PublicUser) UpdatePasswd(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.user SET passwd = $1
		, update_by = $2
		, update_dt = $3
		WHERE user_id = $4`,
		p.Passwd,
		p.UpdateBy,
		p.UpdateDt,
		p.UserID,
	)
	return err
}

func (p *PublicUser) SoftDelete(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.DeleteDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.user SET delete_by = $1
		, delete_dt = $3
		WHERE user_id = $4`,
		p.DeleteBy,
		p.DeleteDt,
		p.UserID,
	)
	return err
}

func (p *PublicUser) Delete(ctx context.Context, tx pgx.Tx) error {
	var err error

	_, err = tx.Exec(ctx, `DELETE FROM public.user WHERE user_id = $1`,
		p.UserID,
	)
	return err
}

func (p *PublicUser) UserRes() UserRes {
	var res UserRes

	// conn, ctx, closeConn := db.GetConnection()
	// defer closeConn()

	res.UserID = p.UserID
	res.Fullname = p.Fullname
	res.NoHp = p.NoHp
	res.Email = p.Email
	res.UserType = p.UserType
	res.PhotoID = p.PhotoID
	res.IsActive = p.IsActive
	// res.PhotoUrl = getPhotoUrl(ctx, conn, p.PhotoID)

	return res
}

func ToUsersRes(users []PublicUser) []UserRes {
	res := make([]UserRes, 0)

	for _, data := range users {
		res = append(res, data.UserRes())
	}

	return res
}
