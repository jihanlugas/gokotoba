//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"context"
	"gokotoba/db"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type KanjiexampleRes struct {
	KanjiexampleID int64  `json:"kanjiexampleId"`
	MskanjiID      int64  `json:"mskanjiId"`
	KanjiID        int64  `json:"kanjiId"`
	UserID         int64  `json:"userId"`
	Kanjiexample   string `json:"kanjiexample"`
	Kana           string `json:"kana"`
	Mean           string `json:"mean"`
	Description    string `json:"description"`
}

func GetKanjiexampleQuery() *db.QueryComposer {
	return db.Query(`SELECT kanjiexample_id, mskanji_id, kanji_id, user_id, kanjiexample, kana, mean, description, create_by, create_dt, update_by, update_dt FROM public.kanjiexample`)
}

func (p *PublicKanjiexample) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetKanjiexampleQuery().
		Where().
		Int64(`kanjiexample_id`, "=", int64(p.KanjiexampleID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func GetKanjiexampleWhere(ctx context.Context, conn *pgxpool.Conn, q *db.QueryBuilder) ([]PublicKanjiexample, error) {
	var err error
	var data []PublicKanjiexample

	err = pgxscan.Select(ctx, conn, &data, q.Build(), q.Params()...)
	if err != nil {
		return data, err
	}
	if len(data) == 0 {
		data = make([]PublicKanjiexample, 0)
	}

	return data, err
}

func (p *PublicKanjiexample) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.kanjiexample (mskanji_id, kanji_id, user_id, kanjiexample, kana, mean, description, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
		RETURNING kanjiexample_id;`,
		p.MskanjiID,
		p.KanjiID,
		p.UserID,
		p.Kanjiexample,
		p.Kana,
		p.Mean,
		p.Description,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.KanjiexampleID)
	return err
}

// mskanji_id kanji_id, user_id not update
func (p *PublicKanjiexample) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.kanjiexample SET kanjiexample = $1
		, kana = $2
		, mean = $3
		, description = $4
		, update_by = $5
		, update_dt = $6
		WHERE kanjiexample_id = $7`,
		p.Kanjiexample,
		p.Kana,
		p.Mean,
		p.Description,
		p.UpdateBy,
		p.UpdateDt,
		p.KanjiexampleID,
	)

	return err
}

func (p *PublicKanjiexample) Res() KanjiexampleRes {
	var res KanjiexampleRes

	res.KanjiexampleID = p.KanjiexampleID
	res.MskanjiID = p.MskanjiID
	res.UserID = p.UserID
	res.KanjiID = p.KanjiID
	res.Kanjiexample = p.Kanjiexample
	res.Kana = p.Kana
	res.Mean = p.Mean
	res.Description = p.Description

	return res
}

func ToKanjiexampleRes(items []PublicKanjiexample) []KanjiexampleRes {
	res := make([]KanjiexampleRes, 0)

	for _, data := range items {
		res = append(res, data.Res())
	}

	return res
}
