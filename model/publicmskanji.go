//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicMskanji struct {
	MskanjiID       int64      `db:"mskanji_id,pk" json:"mskanjiId" form:"mskanjiId" query:"mskanjiId" validate:"required"`
	Kanji           string     `db:"kanji,use_zero" json:"kanji" form:"kanji" query:"kanji" validate:"required,lte=10"`
	ListMeaning     []string   `db:"list_meaning,array,use_zero" json:"listMeaning" form:"listMeaning" query:"listMeaning" validate:"required"`
	ListKunReading  []string   `db:"list_kun_reading,array,use_zero" json:"listKunReading" form:"listKunReading" query:"listKunReading" validate:"required"`
	ListOnReading   []string   `db:"list_on_reading,array,use_zero" json:"listOnReading" form:"listOnReading" query:"listOnReading" validate:"required"`
	ListNameReading []string   `db:"list_name_reading,array,use_zero" json:"listNameReading" form:"listNameReading" query:"listNameReading" validate:"required"`
	Grade           string     `db:"grade,use_zero" json:"grade" form:"grade" query:"grade" validate:"required,lte=10"`
	StrokeCount     string     `db:"stroke_count,use_zero" json:"strokeCount" form:"strokeCount" query:"strokeCount" validate:"required,lte=10"`
	Jlpt            string     `db:"jlpt,use_zero" json:"jlpt" form:"jlpt" query:"jlpt" validate:"required,lte=10"`
	HeisigEn        string     `db:"heisig_en,use_zero" json:"heisigEn" form:"heisigEn" query:"heisigEn" validate:"required,lte=100"`
	Unicode         string     `db:"unicode,use_zero" json:"unicode" form:"unicode" query:"unicode" validate:"required,lte=100"`
	CreateBy        int64      `db:"create_by,use_zero" json:"createBy" form:"createBy" query:"createBy" validate:"required"`
	CreateDt        *time.Time `db:"create_dt,use_zero" json:"createDt" form:"createDt" query:"createDt" validate:"required"`
	UpdateBy        int64      `db:"update_by,use_zero" json:"updateBy" form:"updateBy" query:"updateBy" validate:"required"`
	UpdateDt        *time.Time `db:"update_dt,use_zero" json:"updateDt" form:"updateDt" query:"updateDt" validate:"required"`
}
