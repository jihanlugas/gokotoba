package model

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"gokotoba/db"
)

type fetchKanji struct {
	Kanji           string   `json:"kanji"`
	Grade           int      `json:"grade"`
	StrokeCount     int      `json:"stroke_count"`
	ListMeaning     []string `json:"meanings"`
	ListKunReading  []string `json:"kun_readings"`
	ListOnReading   []string `json:"on_readings"`
	ListNameReading []string `json:"name_readings"`
	Jlpt            int      `json:"jlpt"`
	Unicode         string   `json:"unicode"`
	HeisigEn        string   `json:"heisig_en"`
}

type MskanjiRes struct {
	MskanjiID       int64    `json:"mskanjiId"`
	Kanji           string   `json:"kanji"`
	ListMeaning     []string `json:"listMeaning"`
	ListKunReading  []string `json:"listKunReading"`
	ListOnReading   []string `json:"listOnReading"`
	ListNameReading []string `json:"listNameReading"`
	Grade           string   `json:"grade"`
	StrokeCount     string   `json:"strokeCount"`
	Jlpt            string   `json:"jlpt"`
	HeisigEn        string   `json:"heisigEn"`
	Unicode         string   `json:"unicode"`
}

type MskanjiDetailRes struct {
	MskanjiRes
	ListMskanjiExample []MskanjiexampleRes `json:"listMskanjiExample"`
}

func GetMskanjiQuery() *db.QueryComposer {
	return db.Query(`SELECT mskanji_id, kanji, list_meaning, list_kun_reading, list_on_reading, list_name_reading, grade, stroke_count, jlpt, heisig_en, unicode, create_by, create_dt, update_by, update_dt FROM public.mskanji`)
}

func (p *PublicMskanji) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetMskanjiQuery().
		Where().
		Int64(`mskanji_id`, "=", int64(p.MskanjiID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicMskanji) GetByKanji(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetMskanjiQuery().
		Where().
		StringEq("kanji", p.Kanji).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicMskanji) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.mskanji
		(kanji, list_meaning, list_kun_reading, list_on_reading, list_name_reading, grade, stroke_count, jlpt, heisig_en, unicode, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
		RETURNING mskanji_id;`,
		p.Kanji,
		p.ListMeaning,
		p.ListKunReading,
		p.ListOnReading,
		p.ListNameReading,
		p.Grade,
		p.StrokeCount,
		p.Jlpt,
		p.HeisigEn,
		p.Unicode,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.MskanjiID)

	return err
}

// kanji not update
func (p *PublicMskanji) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.mskanji SET list_meaning = $1
		, list_kun_reading = $2
		, list_on_reading = $3
		, list_name_reading = $4
		, grade = $5
		, stroke_count = $6
		, jlpt = $7
		, heisig_en = $8
		, unicode = $9
		, update_by = $10
		, update_dt = $11
		WHERE mskanji_id = $12`,
		p.ListMeaning,
		p.ListKunReading,
		p.ListOnReading,
		p.ListNameReading,
		p.Grade,
		p.StrokeCount,
		p.Jlpt,
		p.HeisigEn,
		p.Unicode,
		p.UpdateBy,
		p.UpdateDt,
		p.MskanjiID,
	)
	return err
}

func (p *PublicMskanji) Delete(ctx context.Context, tx pgx.Tx) error {
	var err error

	_, err = tx.Exec(ctx, `DELETE FROM public.mskanji WHERE mskanji_id = $1`,
		p.MskanjiID,
	)
	return err
}

func (p *PublicMskanji) FetchKanji() error {
	var err error
	url := "https://kanjiapi.dev/v1/kanji/" + p.Kanji
	spaceClient := http.Client{
		Timeout: time.Second * 10, // Timeout after 2 seconds
	}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Accept", "application/json")
	res, err := spaceClient.Do(req)
	if err != nil {
		return err
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	newKanji := fetchKanji{}
	err = json.Unmarshal(body, &newKanji)
	if err != nil {
		return err
	}

	p.Kanji = newKanji.Kanji
	p.Grade = strconv.Itoa(newKanji.Grade)
	p.StrokeCount = strconv.Itoa(newKanji.StrokeCount)
	p.ListMeaning = newKanji.ListMeaning
	p.ListKunReading = newKanji.ListKunReading
	p.ListOnReading = newKanji.ListOnReading
	p.ListNameReading = newKanji.ListNameReading
	p.Jlpt = strconv.Itoa(newKanji.Jlpt)
	p.Unicode = newKanji.Unicode
	p.HeisigEn = newKanji.HeisigEn

	return nil
}

func (p *PublicMskanji) Res() MskanjiRes {
	var res MskanjiRes

	res.MskanjiID = p.MskanjiID
	res.Kanji = p.Kanji
	res.ListMeaning = p.ListMeaning
	res.ListKunReading = p.ListKunReading
	res.ListOnReading = p.ListOnReading
	res.ListNameReading = p.ListNameReading
	res.Grade = p.Grade
	res.StrokeCount = p.StrokeCount
	res.Jlpt = p.Jlpt
	res.HeisigEn = p.HeisigEn
	res.Unicode = p.Unicode

	return res
}
