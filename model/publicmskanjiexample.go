//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type PublicMskanjiexample struct {
	MskanjiexampleID int64      `db:"mskanjiexample_id,pk" json:"mskanjiexampleId" form:"mskanjiexampleId" query:"mskanjiexampleId" validate:"required"`
	MskanjiID        int64      `db:"mskanji_id,use_zero" json:"mskanjiId" form:"mskanjiId" query:"mskanjiId" validate:"required"`
	Kanjiexample     string     `db:"kanjiexample,use_zero" json:"kanjiexample" form:"kanjiexample" query:"kanjiexample" validate:"required,lte=80"`
	Kana             string     `db:"kana,use_zero" json:"kana" form:"kana" query:"kana" validate:"required,lte=80"`
	Mean             string     `db:"mean,use_zero" json:"mean" form:"mean" query:"mean" validate:"required,lte=80"`
	Description      string     `db:"description,use_zero" json:"description" form:"description" query:"description" validate:"required,lte=500"`
	CreateBy         int64      `db:"create_by,use_zero" json:"createBy" form:"createBy" query:"createBy" validate:"required"`
	CreateDt         *time.Time `db:"create_dt,use_zero" json:"createDt" form:"createDt" query:"createDt" validate:"required"`
	UpdateBy         int64      `db:"update_by,use_zero" json:"updateBy" form:"updateBy" query:"updateBy" validate:"required"`
	UpdateDt         *time.Time `db:"update_dt,use_zero" json:"updateDt" form:"updateDt" query:"updateDt" validate:"required"`
}
