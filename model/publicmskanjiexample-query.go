package model

import (
	"context"
	"gokotoba/db"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type MskanjiexampleRes struct {
	MskanjiexampleID int64  `json:"mskanjiexampleId"`
	MskanjiID        int64  `json:"mskanjiId"`
	Kanjiexample     string `json:"kanjiexample"`
	Kana             string `json:"kana"`
	Mean             string `json:"mean"`
	Description      string `json:"description"`
}

func GetMskanjiexampleQuery() *db.QueryComposer {
	return db.Query(`SELECT mskanjiexample_id, mskanji_id, kanjiexample, kana, mean, description, create_by, create_dt, update_by, update_dt FROM public.mskanjiexample`)
}

func (p *PublicMskanjiexample) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetMskanjiexampleQuery().
		Where().
		Int64(`mskanjiexample_id`, "=", int64(p.MskanjiexampleID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func GetMskanjiexampleWhere(ctx context.Context, conn *pgxpool.Conn, q *db.QueryBuilder) ([]PublicMskanjiexample, error) {
	var err error
	var data []PublicMskanjiexample

	err = pgxscan.Select(ctx, conn, &data, q.Build(), q.Params()...)
	if err != nil {
		return data, err
	}
	if len(data) == 0 {
		data = make([]PublicMskanjiexample, 0)
	}

	return data, err
}

func (p *PublicMskanjiexample) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.mskanjiexample (mskanji_id, kanjiexample, kana, mean, description, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
		RETURNING mskanjiexample_id;`,
		p.MskanjiID,
		p.Kanjiexample,
		p.Kana,
		p.Mean,
		p.Description,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.MskanjiexampleID)
	return err
}

// mskanji not update
func (p *PublicMskanjiexample) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.mskanjiexample SET kanjiexample = $1
		, kana = $2
		, mean = $3
		, description = $4
		, update_by = $5
		, update_dt = $6
		WHERE mskanjiexample_id = $7`,
		p.Kanjiexample,
		p.Kana,
		p.Mean,
		p.Description,
		p.UpdateBy,
		p.UpdateDt,
		p.MskanjiexampleID,
	)

	return err
}

func (p *PublicMskanjiexample) Res() MskanjiexampleRes {
	var res MskanjiexampleRes

	res.MskanjiexampleID = p.MskanjiexampleID
	res.MskanjiID = p.MskanjiID
	res.Kanjiexample = p.Kanjiexample
	res.Kana = p.Kana
	res.Mean = p.Mean
	res.Description = p.Description

	return res
}

func ToMskanjiexampleRes(items []PublicMskanjiexample) []MskanjiexampleRes {
	res := make([]MskanjiexampleRes, 0)

	for _, data := range items {
		res = append(res, data.Res())
	}

	return res
}
