package model

import (
	"context"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"gokotoba/db"
)

type KanjiRes struct {
	KanjiID         int64    `json:"kanjiId"`
	MskanjiID       int64    `json:"mskanjiId"`
	UserID          int64    `json:"userId"`
	Kanji           string   `json:"kanji"`
	ListMeaning     []string `json:"listMeaning"`
	ListKunReading  []string `json:"listKunReading"`
	ListOnReading   []string `json:"listOnReading"`
	ListNameReading []string `json:"listNameReading"`
	Grade           string   `json:"grade"`
	StrokeCount     string   `json:"strokeCount"`
	Jlpt            string   `json:"jlpt"`
	HeisigEn        string   `json:"heisigEn"`
	Unicode         string   `json:"unicode"`
}

type KanjiDetailRes struct {
	KanjiRes
	ListKanjiExample []KanjiexampleRes `json:"listKanjiExample"`
}

func GetKanjiQuery() *db.QueryComposer {
	return db.Query(`SELECT kanji_id, mskanji_id, user_id, kanji, list_meaning, list_kun_reading, list_on_reading, list_name_reading, grade, stroke_count, jlpt, heisig_en, unicode, create_by, create_dt, update_by, update_dt FROM public.kanji`)
}

func (p *PublicKanji) GetById(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetKanjiQuery().
		Where().
		Int64(`kanji_id`, "=", int64(p.KanjiID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicKanji) GetByKanji(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetKanjiQuery().
		Where().
		Int64("user_id", "=", p.UserID).
		StringEq(`kanji`, p.Kanji).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicKanji) GetByUserMskanji(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := GetKanjiQuery().
		Where().
		Int64("mskanji_id", "=", p.MskanjiID).
		Int64("user_id", "=", p.UserID).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func GetKanjiWhere(ctx context.Context, conn *pgxpool.Conn, q *db.QueryBuilder) ([]PublicKanji, error) {
	var err error
	var data []PublicKanji

	err = pgxscan.Select(ctx, conn, &data, q.Build(), q.Params()...)
	if err != nil {
		return data, err
	}
	if len(data) == 0 {
		data = make([]PublicKanji, 0)
	}

	return data, err
}

func (p *PublicKanji) Insert(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.CreateDt = &now
	p.UpdateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.kanji
		(mskanji_id, user_id, kanji, list_meaning, list_kun_reading, list_on_reading, list_name_reading, grade, stroke_count, jlpt, heisig_en, unicode, create_by, create_dt, update_by, update_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16)
		RETURNING kanji_id;`,
		p.MskanjiID,
		p.UserID,
		p.Kanji,
		p.ListMeaning,
		p.ListKunReading,
		p.ListOnReading,
		p.ListNameReading,
		p.Grade,
		p.StrokeCount,
		p.Jlpt,
		p.HeisigEn,
		p.Unicode,
		p.CreateBy,
		p.CreateDt,
		p.UpdateBy,
		p.UpdateDt,
	).Scan(&p.KanjiID)

	return err
}

// mskanji_id, user_id, kanji not update
func (p *PublicKanji) Update(ctx context.Context, tx pgx.Tx) error {
	var err error

	now := time.Now()
	p.UpdateDt = &now
	_, err = tx.Exec(ctx, `UPDATE public.kanji SET list_meaning = $1
		, list_kun_reading = $2
		, list_on_reading = $3
		, list_name_reading = $4
		, grade = $5
		, stroke_count = $6
		, jlpt = $7
		, heisig_en = $8
		, unicode = $9
		, update_by = $10
		, update_dt = $11
		WHERE kanji_id = $12`,
		p.ListMeaning,
		p.ListKunReading,
		p.ListOnReading,
		p.ListNameReading,
		p.Grade,
		p.StrokeCount,
		p.Jlpt,
		p.HeisigEn,
		p.Unicode,
		p.UpdateBy,
		p.UpdateDt,
		p.KanjiID,
	)
	return err
}

func (p *PublicKanji) Delete(ctx context.Context, tx pgx.Tx) error {
	var err error

	_, err = tx.Exec(ctx, `DELETE FROM public.kanji WHERE kanji_id = $1`,
		p.KanjiID,
	)
	return err
}

func (p *PublicKanji) Res() KanjiRes {
	var res KanjiRes

	res.KanjiID = p.KanjiID
	res.UserID = p.UserID
	res.MskanjiID = p.MskanjiID
	res.Kanji = p.Kanji
	res.ListMeaning = p.ListMeaning
	res.ListKunReading = p.ListKunReading
	res.ListOnReading = p.ListOnReading
	res.ListNameReading = p.ListNameReading
	res.Grade = p.Grade
	res.StrokeCount = p.StrokeCount
	res.Jlpt = p.Jlpt
	res.HeisigEn = p.HeisigEn
	res.Unicode = p.Unicode

	return res
}

func ToKanjiRes(items []PublicKanji) []KanjiRes {
	res := make([]KanjiRes, 0)

	for _, data := range items {
		res = append(res, data.Res())
	}

	return res
}
