//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"context"
	"errors"
	"gokotoba/config"
	"gokotoba/constant"
	"gokotoba/db"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func (p *PublicPhoto) GetByID(ctx context.Context, conn *pgxpool.Conn) error {
	var err error

	sql := db.Query(`SELECT photo_id, client_name, server_name, ext, photo_path, photo_size, photo_width, photo_heigth, create_by, create_dt FROM public.item`).
		Where().
		Int64(`photo_id`, "=", int64(p.PhotoID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, conn, p, sql.Build(), sql.Params()...)

	return err
}

func (p *PublicPhoto) Upload(ctx context.Context, tx pgx.Tx, file *multipart.FileHeader, docType constant.DocType, userID int64) error {
	// asumsi penggunaan fungsi ini adalah check file (berapa mb? extension bener/tidak? dll) sudah dilakukan sebelumnya.
	var err error
	var newPhotoID int64
	now := time.Now()

	err = tx.QueryRow(ctx, `select public.next_id() as tid;`).Scan(&newPhotoID)
	if err != nil {
		return err
	}

	p.PhotoID = newPhotoID
	p.ClientName = file.Filename
	p.Ext = filepath.Ext(file.Filename)
	p.ServerName = strconv.FormatInt(int64(newPhotoID), 10) + p.Ext
	p.PhotoPath = constant.FolderForFile[docType]
	p.PhotoSize = file.Size
	p.PhotoWidth = 0
	p.PhotoHeigth = 0
	p.CreateBy = userID
	p.CreateDt = &now
	err = tx.QueryRow(ctx, `INSERT INTO public.photo (photo_id, client_name, server_name, ext, photo_path, photo_size, photo_width, photo_heigth, create_by, create_dt)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
		RETURNING photo_id;`,
		p.PhotoID,
		p.ClientName,
		p.ServerName,
		p.Ext,
		p.PhotoPath,
		p.PhotoSize,
		p.PhotoWidth,
		p.PhotoHeigth,
		p.CreateBy,
		p.CreateDt,
	).Scan(&p.PhotoID)
	if err != nil {
		return err
	}

	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Destination
	dst, err := os.Create(config.UploadFilePath + "/" + constant.FolderForFile[docType] + "/" + p.ServerName)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}
	return nil

}

func (p *PublicPhoto) Delete(ctx context.Context, tx pgx.Tx) error {
	var err error

	sql := db.Query(`SELECT photo_id, client_name, server_name, ext, photo_path, photo_size, photo_width, photo_heigth, create_by, create_dt FROM public.photo`).
		Where().
		Int64(`photo_id`, "=", int64(p.PhotoID)).
		OffsetLimit(0, 1)
	err = pgxscan.Get(ctx, tx, p, sql.Build(), sql.Params()...)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			err = errors.New("Data not found")
		}
		return err
	}

	if err = os.Remove(config.UploadFilePath + "/" + p.PhotoPath + "/" + p.ServerName); err != nil {
		return err
	}
	_, err = tx.Exec(ctx, "DELETE FROM public.photo WHERE photo_id = $1", p.PhotoID)

	return err
}

//
//func (p *PublicPhoto) Insert(ctx context.Context, tx pgx.Tx) error {
//	var err error
//
//	now := time.Now()
//	p.CreateDt = &now
//	err = tx.QueryRow(ctx, `INSERT INTO public.item (photo_id, client_name, server_name, ext, photo_path, photo_size, photo_width, photo_heigth, create_by, create_dt)
//		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
//		RETURNING photo_id;`,
//		p.PhotoID,
//		p.ClientName,
//		p.ServerName,
//		p.Ext,
//		p.PhotoPath,
//		p.PhotoSize,
//		p.PhotoWidth,
//		p.PhotoHeigth,
//		p.CreateBy,
//		p.CreateDt,
//	).Scan(&p.PhotoID)
//	return err
//}

func getPhotoUrl(ctx context.Context, conn *pgxpool.Conn, photoId int64) string {
	if photoId == 0 {
		return ""
	} else {
		var photo PublicPhoto
		var err error
		sql := db.Query("SELECT photo_id, client_name, server_name, ext, photo_path, photo_size, photo_width, photo_heigth FROM public.photo").
			Where().
			Int64("photo_id", "=", int64(photoId))
		err = pgxscan.Get(ctx, conn, &photo, sql.Build(), sql.Params()...)
		if err != nil {
			return err.Error()
		}
		return config.UrlAccessUploadFile + "/" + photo.PhotoPath + "/" + photo.ServerName
	}
}
